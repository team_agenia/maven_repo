import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.*;

/**
 * <p>Created: 18-04-2018</p>
 * <p>author: Runi</p>
 */

public class Test {

    public static void main(String[] args) {
       /* List<String> strippedSegments = new ArrayList<>(Arrays.asList("1"));

        int indexHigh = strippedSegments.size()-1;

        for(int indexLow = 0; indexLow < indexHigh; indexLow++){
            String segmentLow = strippedSegments.get(indexLow);
            String segmentHigh = strippedSegments.get(indexHigh);
            strippedSegments.set(indexHigh--, segmentLow);
            strippedSegments.set(indexLow++, segmentHigh);
        }

        for(String s : strippedSegments) System.out.println(s);*/

        List<String> listTest = new ArrayList<>();

        listTest.add("document/{id}/album/{aid}");
        listTest.add("document");
        listTest.add("document/{id}/album");
        listTest.add("document/{id}");

        listTest.sort(Comparator.comparingInt(String::length));

        System.out.printf("test");

        String resource = "clients/{id}/projects/{pid}";

        System.out.println(resource.replaceAll("\\{.*?\\}", "\\*"));

        String resourceRegex = "^" + resource.replace("/", "\\/") + "$";
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.putSingle("id", "cli1");
        params.putSingle("pid", "pro2");
        for (String key : params.keySet()) {
            String param = params.getFirst(key);
            resourceRegex = resourceRegex.replace("{" + key + "}", "\\*|(" + param + ")");
        }

        System.out.println(resourceRegex);

    }
}
