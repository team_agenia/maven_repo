package dk.agenia.permissionmanagement;

import javax.ws.rs.core.Response;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */

public class RestException extends Exception {

    private Response.Status status;
    public RestException(String message, Response.Status status){
        super(message);
        this.status = status;
    }

    public Response.Status getStatus(){
        return this.status;
    }
}
