package dk.agenia.permissionmanagement.models;

import lombok.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created: 26-04-2018
 * author: Runi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Resource {

    private String name;
    private List<String> descendants;
    private Set<String> permissions;

    /**
     * @return the resource name with all the path params replaced with '*'
     */
    public String getNameWithWildcardParams() {
        return addWildcards(name);
    }

    public List<String> getDescendantsWithWildcardParams() {
        List<String> values = new ArrayList<>();
        for (String s : descendants) {
            values.add(addWildcards(s));
        }
        return values;
    }

    public String getNameWithParams(Param... params) {
        String value = this.name;
        for (Param p : params) {
            value = addParam(value, p);
        }
        value = addWildcards(value);
        return value;
    }

    public List<String> getDescendantsWithParams(Param... params) {
        List<String> values = new ArrayList<>();
        for (String desc : descendants) {
            String value = desc;
            for (Param p : params) value = addParam(value, p);
            value = addWildcards(value);
            values.add(value);
        }
        return values;
    }

    public List<String> getSelfAndDescendants() {
        List<String> list = new ArrayList<>();
        list.add(this.getName());
        if (this.getDescendants() != null) list.addAll(this.getDescendants());
        return list;
    }

    public List<String> getSelfAndDescendantsWithParams(Param... params) {
        List<String> list = new ArrayList<>();
        list.add(this.getNameWithParams(params));
        if (this.getDescendants() != null) list.addAll(this.getDescendantsWithParams(params));

        return list;
    }

    private String addWildcards(String value) {
        return value.replaceAll("\\{.*?\\}", "*");
    }

    private String addParam(String value, Param param) {
        return value.replace("{" + param.key + "}", param.value);
    }

    public static class Param {
        public String key;
        public String value;

        public Param(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
