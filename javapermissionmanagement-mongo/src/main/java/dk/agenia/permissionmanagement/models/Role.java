package dk.agenia.permissionmanagement.models;

import lombok.*;

import java.util.*;

/**
 * Created: 14-04-2018
 * author: Runi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Role {
    private String parentId;
    private String name;
    private List<String> ancestors;

    public String getRolePath(){
        StringBuilder value = new StringBuilder("," + this.getName() + ",");
        for(String r : this.getAncestors()){
            value.append(r).append(",");
        }
        return value.toString();
    }
}
