package dk.agenia.permissionmanagement.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

/**
 * Created: 14-04-2018
 * Author: Runi
 * <p>
 * Glorified extensible enum -
 */

public class Permission  {
    public static final String CREATE = "Create";
    public static final String READ = "READ";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";
    public static final String ADMIN = "ADMIN";
}
