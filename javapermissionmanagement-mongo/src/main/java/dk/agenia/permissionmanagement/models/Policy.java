package dk.agenia.permissionmanagement.models;

import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created: 20-04-2018
 * author: Runi
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Policy  {

    private String id;
    private String roleId;
    private String targetResource;
    private transient Map<String, String> params;
    private List<String> resources;
    private Set<String> permissions;
}
