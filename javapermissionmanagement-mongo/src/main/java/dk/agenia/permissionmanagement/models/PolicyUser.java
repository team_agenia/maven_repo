package dk.agenia.permissionmanagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PolicyUser extends BaseModel  {

    private Set<String> roles;
    private String username;
    @JsonIgnore
    private String hash;

    public PolicyUser claims() {
        UserClaims claims = new UserClaims();
        claims.setUsername(username);
        claims.setId(getId());
        return claims;
    }

    public Set<String> getRolesFormatted(){
        Set<String> formatted = new HashSet<>();
        if(roles == null) return formatted;
        for(String role : roles){
            String[] parts = role.split(",");
            for(String part : parts){
                if(!part.equals("")){
                    formatted.add(part);
                }
            }
        }
        return formatted;
    }
}
