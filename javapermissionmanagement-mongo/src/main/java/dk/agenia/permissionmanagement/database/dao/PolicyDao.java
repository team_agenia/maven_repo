package dk.agenia.permissionmanagement.database.dao;

import java.util.List;
import java.util.Map;

/**
 * Created: 20-09-2018
 * author: Runi
 */

public interface PolicyDao {


    boolean hasAccess(String resource, List<String> roles, List<Map<String, String>> params);

}
