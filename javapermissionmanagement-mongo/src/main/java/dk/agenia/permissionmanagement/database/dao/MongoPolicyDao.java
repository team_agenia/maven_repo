package dk.agenia.permissionmanagement.database.dao;

import java.util.List;
import java.util.Map;

/**
 * Created: 21-09-2018
 * author: Runi
 */

public class MongoPolicyDao implements PolicyDao {

    public MongoPolicyDao(){

    }

    @Override
    public boolean hasAccess(String resource, List<String> roles, List<Map<String, String>> params) {
        return false;
    }
}
