package dk.agenia.permissionmanagement.controllers;

import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.models.Policy;
import dk.agenia.permissionmanagement.models.PolicyUser;
import dk.agenia.permissionmanagement.models.Resource;
import dk.agenia.permissionmanagement.models.Role;

import javax.ws.rs.Path;
import java.lang.reflect.Method;
import java.util.*;

import static dk.agenia.permissionmanagement.utils.PermissionHelper.*;

/**
 * Created: 21-09-2018
 * author: Runi
 */

public abstract class ResourceController<U extends PolicyUser, P extends Policy, S extends Resource, R extends Role> {


    public Set<S> generateResources(Class<?>[] classes, String prefix) {
        Map<String, S> map = new HashMap<>();
        new HashSet<>(map.values());
        if (classes == null) return new HashSet<>();
        for (Class<?> aClass : classes) {
            //if class has annotation it is not a subclass. Subclasses are fetched when processing their parent class
            if (isAnnotatedResourceClass(aClass) && !(prefix == null && aClass.isMemberClass())) {
                processResources(aClass, map);
            }
        }
        Set<S> set = new HashSet<>();
        for (S first : map.values()) {
            for (S second : map.values()) {
                if (!first.getName().equals(second.getName())) {
                    if (first.getName().startsWith(second.getName())) { //second is parent
                        second.getDescendants().add(first.getName());
                    }
                }
            }
            //sort descendants from child -> grandchild
            first.getDescendants().sort(Comparator.comparingInt(String::length));
            set.add(first);
        }
        return set;
    }

    /**
     * <p>This method should <strong>NOT save anything to database</strong></p>
     * This method is made abstract to give developer option to extend the Resource object.
     *
     * @param resource          the @Path value for the resource
     * @param activePermissions the possible permissions a user can enable for this resource
     * @return an object that extends {@link Resource} where name is the resource argument and permissions is the activePermissions
     */
    public abstract S createResourceObject(String resource, Set<String> activePermissions);


    private void processResources(Class<?> methodClass, Map<String, S> resourceMap) {

        Secured securedAnnotation = getSecuredAnnotation(methodClass);
        Path pathAnnotation = methodClass.getAnnotation(Path.class);
        String path = pathAnnotation.value();
        path = trimSlashes(path);

        //String classResource = pathAnnotation.value();

        S currentResource = resourceMap.get(path);//Resource.builder().build();
//        if (parentResource != null) {
//            currentResource.setName(generateResource(parentResource.getName(), pathAnnotation.value()));
//        }else{
        if (currentResource == null) {
            currentResource = createResourceObject(path, new HashSet<>());
            resourceMap.put(currentResource.getName(), currentResource);
        }
//        }

        boolean classUseDefault = false; //not necessarily secured thus per default do not use default permissions
        Set<String> classActivePermissions = new HashSet<>();

        if (securedAnnotation != null) {
            classUseDefault = securedAnnotation.useDefault();
            classActivePermissions.addAll(Arrays.asList(securedAnnotation.requires()));
        }

        for (Method method : getMethods(methodClass)) {
            boolean methodUseDefault = classUseDefault;
            if (isMethodEndpoint(method)) {
                //compute the name of the resource
                String methodResource = currentResource.getName();
                Path methodPathAnnotation = method.getAnnotation(Path.class);
                if (methodPathAnnotation != null) {
                    methodResource = generateResource(currentResource.getName(), methodPathAnnotation.value());
                }

                //compute which permissions are available for this resource - add admin permissions per default
                Set<String> methodActivePermissions = new HashSet<>();
                Secured methodSecuredAnnotation = method.getAnnotation(Secured.class);
                if (methodSecuredAnnotation != null) {
                    methodUseDefault = methodSecuredAnnotation.useDefault();
                    methodActivePermissions.addAll(Arrays.asList(methodSecuredAnnotation.requires()));
                    if (methodUseDefault) //stack permissions if using default
                        methodActivePermissions.addAll(classActivePermissions);
                } else {
                    methodActivePermissions.addAll(classActivePermissions);
                }

                if (methodUseDefault) {
                    String defaultPermission = getDefaultPermission(method);
                    if (defaultPermission != null) methodActivePermissions.add(defaultPermission);
                }

//                if (parentResource != null) {
                //TODO incomment to support nested classes - this needs more work - look for similar comment that is for same functionality
                    /*String finalMethodResource = methodResource;
                    //TODO it should probably not be possible to have two innerclasses with same path? or is there another scenario where resource might already exist?
                    Optional<Resource> existing = parentResource.getSubResources().stream().filter(r -> r.getName().equals(finalMethodResource)).findFirst();
                    if (existing.isPresent()) {
                        existing.get().getActivePermissions().addAll(methodActivePermissions);
                    } else {
                        parentResource.getSubResources().add(
                                Resource.builder()
                                        .name(methodResource)
                                        .activePermissions(methodActivePermissions)
                                        .build());
                    }*/
//                } else {
                //if subresource add it to parent
                if (!methodResource.equals(currentResource.getName())) {
                    String finalMethodResource1 = methodResource;
//                    Optional<Resource> existing = currentResource.getSubResources().stream().filter(r -> r.getName().equals(finalMethodResource1)).findFirst();
                    S existing = resourceMap.get(methodResource);
                    if (existing != null) {
                        existing.getPermissions().addAll(methodActivePermissions);
                    } else {
//                        currentResource.getSubResources().add(Resource.builder().name(methodResource).activePermissions(methodActivePermissions).build());
                        resourceMap.put(methodResource, createResourceObject(methodResource, methodActivePermissions));
                    }
                } else {
                    //methodResource == classResource
                    currentResource.getPermissions().addAll(methodActivePermissions);
                }
//                }
            }
        }

        //TODO incomment to support nested classes - this needs more work - look for similar comment that is for same functionality
        /*for (Class<?> aClass : methodClass.getClasses()) {
            processResources(aClass, resourceMap, currentResource);
        }*/

    }


    public String generateResource(String prefix, String resource) {
        if (prefix == null) return resource;
        if (prefix.endsWith("/")) prefix = prefix.substring(0, prefix.length() - 1);
        if (!resource.startsWith("/")) resource = "/" + resource;
        return prefix + resource;
    }


    private boolean isAnnotatedResourceClass(Class rc) {
        if (rc.isAnnotationPresent(Path.class)) {
            return true;
        }

        for (Class i : rc.getInterfaces()) {
            if (i.isAnnotationPresent(Path.class)) {
                return true;
            }
        }
        return false;
    }

}
