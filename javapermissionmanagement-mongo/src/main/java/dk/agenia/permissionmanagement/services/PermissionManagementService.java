package dk.agenia.permissionmanagement.services;

import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.models.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

import static dk.agenia.permissionmanagement.models.Permission.*;

/**
 * Created: 26-04-2018
 * author: Runi
 */
/*
@Secured(requires = Permission.ADMIN)*/
@Path("management")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class PermissionManagementService<U extends PolicyUser, P extends Policy, S extends Resource, R extends Role> {

    @Context
    Application application;

    /**
     *
     * @param update
     * @return
     * @throws Exception
     */
    @GET
    @Path("resources")
    public abstract Set<S> getResources(@QueryParam("update") String update) throws Exception;


    /**
     *
     * @return
     * @throws Exception
     */
    @GET
    @Path("permissions")
    public List<String> getPermissions() throws Exception {
        return Arrays.asList(CREATE, READ, UPDATE, DELETE, ADMIN);
    }

    /**
     *
     * @param query
     * @param page
     * @param size
     * @param orderBy
     * @param order
     * @param includeUserRoles
     * @return
     * @throws Exception
     */
    @GET
    @Path("roles")
    public abstract ListWithTotal<R> getRoles(@QueryParam("query") String query,
                                     @QueryParam("page") @DefaultValue("0") int page,
                                     @QueryParam("size") @DefaultValue("-1") int size,
                                     @QueryParam("orderBy") @DefaultValue("name") String orderBy,
                                     @QueryParam("order") @DefaultValue("asc") String order,
                                     @QueryParam("includeUserRoles") @DefaultValue("false") boolean includeUserRoles) throws Exception;

    /**
     *
     * @param role
     * @return
     * @throws Exception
     */
    @POST
    @Path("roles")
    public abstract R createRole(R role) throws Exception;

    @GET
    @Path("roles/{id}")
    public abstract R getRole(@PathParam("id") String id) throws Exception;

    @PUT
    @Path("roles/{id}")
    public abstract R updateRole(@PathParam("id") String id, R role) throws Exception;

    @DELETE
    @Path("roles/{id}")
    public abstract void deleteRole(@PathParam("id") String id, R role) throws Exception;

    @POST
    @Path("roles/{id}/policies")
    public abstract P createPolicyForRole(@PathParam("id") String roleId, P policy) throws Exception;

    @GET
    @Path("roles/{id}/policies")
    public abstract List<P> getPoliciesForRole(@PathParam("id") String roleId) throws Exception;

    @PUT
    @Path("roles/{id}policies/{pid}")
    public abstract P updatePolicy(@PathParam("id") String roleId, @PathParam("pid") String policyId, P policy) throws Exception;

    @DELETE
    @Path("roles/{id}/policies/{pid}")
    public abstract void deletePolicy(@PathParam("id") String roleId, @PathParam("pid") String policyId, P policy) throws Exception;

    @GET
    @Path("users")
    public abstract ListWithTotal<U> getUsers(@QueryParam("query") String query,
                                              @QueryParam("page") @DefaultValue("0") int page,
                                              @QueryParam("size") @DefaultValue("-1") int size,
                                              @QueryParam("orderBy") @DefaultValue("username") String orderBy,
                                              @QueryParam("order") @DefaultValue("asc") String order) throws Exception;

    @POST
    @Path("users")
    public abstract U createUser(U user) throws Exception;

    @GET
    @Path("users/{id}")
    public abstract U getUser(@PathParam("id") String id) throws Exception;

    @PUT
    @Path("users/{id}")
    public abstract U updateUser(@PathParam("id") String id, U user) throws Exception;

    @DELETE
    @Path("users/{id}")
    public abstract void deleteUser(@PathParam("id") String id, U user) throws Exception;
}
