package dk.agenia.permissionmanagement.filters;

import dk.agenia.permissionmanagement.RestException;
import dk.agenia.permissionmanagement.annotations.OwnerAccess;
import dk.agenia.permissionmanagement.controllers.ResourceController;
import dk.agenia.permissionmanagement.models.PolicyUser;
import dk.agenia.permissionmanagement.models.Role;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static dk.agenia.permissionmanagement.utils.PermissionHelper.getActivePermissions;
import static java.util.Arrays.asList;

/**
 * Created: 16-05-2018
 * author: Runi
 *
 * <p>
 *     This filter will create a policy for the current user when an endpoint is called which is annotated with the {@link OwnerAccess}
 *     annotation.
 * </p>
 */
@Slf4j
public abstract class BaseOwnerAccessFilter<T extends PolicyUser, R extends Role> implements ContainerResponseFilter {
    @Context
    ResourceInfo resourceInfo;
    @Context
    HttpHeaders headers;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        if (resourceInfo.getResourceMethod() == null) {
            log.error("Could not identify resource method");
            return;
        }
        //check if OwnerAccess annotation exists otherwise stop
        OwnerAccess ownerAnnotation = resourceInfo.getResourceMethod().getAnnotation(OwnerAccess.class);
        if (ownerAnnotation == null) return;

        //resolve the Path annotation - if the method is not annotated we assume that the class is annotated
        Path pathAnnotation = resourceInfo.getResourceMethod().getAnnotation(Path.class);
        if (pathAnnotation == null) pathAnnotation = resourceInfo.getResourceClass().getAnnotation(Path.class);

        Object entity = responseContext.getEntity();
        String id = getId(entity, ownerAnnotation.idMethod());
        if (id == null) return;



        //calculate the prefix of the final resource which we want to create a policy for
        //per default use the @Path value of the method or if @OwnerAccess states different use that value instead.
        String parentResource;
        if(ownerAnnotation.targetResource().equals("")){
            parentResource = pathAnnotation.value();
        }else{
            parentResource = ownerAnnotation.targetResource();
        }
        if(!parentResource.endsWith("/")) parentResource = parentResource + "/";
        String targetResource = parentResource + id;
        String targetGenericResource = parentResource + ownerAnnotation.parameterValue();

        //create specific resource
        MultivaluedMap<String, String> params = requestContext.getUriInfo().getPathParameters();
        for(String paramKey: params.keySet()){
            targetResource = targetResource.replace("{" + paramKey + "}", params.getFirst(paramKey));
        }
        //replace additional parameters with wildcard value
        targetResource = targetResource.replaceAll("\\{.*?\\}", "\\*");

        R userRole = null;
        try {
            userRole = getUserRole(requestContext);
        } catch (RestException e) {
            log.error("could not get user role", e);
        }

        try {
            grantAccess(userRole, targetGenericResource, targetResource, new HashSet<>(asList(ownerAnnotation.permissions())));
        } catch (Exception e) {
            log.error("Could not grant user access", e);
        }
    }

    public abstract ResourceController getResourceController();

    public String getId(Object entity, String idMethod) {
        String id = null;
        try {
            Method method = entity.getClass().getMethod(idMethod);
            Object idValue = method.invoke(entity);
            id = idValue.toString();
        } catch (NoSuchMethodException e) {
            log.warn("id method not found", e);
        } catch (IllegalAccessException e) {
            log.warn("Illegal access on id method", e);
        } catch (InvocationTargetException e) {
            log.warn("id method invocation exception", e);
        }
        return id;
    }

    public abstract void grantAccess(
            R userRole,
            String genericResource,
            String targetResource,
            Set<String> allowedPermissions) throws Exception;

    public abstract R getUserRole(ContainerRequestContext requestContext) throws RestException;
}