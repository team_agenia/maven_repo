package dk.agenia.permissionmanagement.filters;

import dk.agenia.permissionmanagement.RestError;
import dk.agenia.permissionmanagement.RestException;
import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.models.*;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static dk.agenia.permissionmanagement.models.Permission.*;
import static dk.agenia.configservice.Configuration.get;
import static java.util.Arrays.asList;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */
@Slf4j
public abstract class BaseAuthenticationFilter<U extends PolicyUser> implements ContainerRequestFilter {

    private final String USER_CONTEXT_KEY = get("CONTEXT_USER_KEY", "user");


    @Context
    ResourceInfo resourceInfo;
    @Context
    HttpHeaders headers;


    public void filter(ContainerRequestContext requestContext) throws IOException {
        //options requests are not filtered
        if ("options".equals(requestContext.getMethod().toLowerCase())) return;

        U user;
        try {
            user = getUser(requestContext);
        } catch (RestException e) {
            error(requestContext, e.getStatus(), e.getMessage());
            return;
        }
        processRequest(requestContext, user);
        if (user != null)
            requestContext.setProperty(USER_CONTEXT_KEY, user);
    }

    @SuppressWarnings("unchecked")
    public void processRequest(ContainerRequestContext requestContext, U user) throws IOException {


        Secured methodAnnotation = resourceInfo.getResourceMethod().getAnnotation(Secured.class);
        Secured classAnnotation = resourceInfo.getResourceClass().getAnnotation(Secured.class);
        if (methodAnnotation == null && classAnnotation == null) return;

        if (user == null) {
            error(requestContext, Response.Status.FORBIDDEN, "permission denied");
            return;
        }

        boolean useDefault = false;
        boolean disabled = false;
        Set<String> requiredPermissions = new HashSet<>();

        if(classAnnotation != null){
            useDefault = classAnnotation.useDefault();
            disabled = classAnnotation.disable();
            requiredPermissions.addAll(asList(classAnnotation.requires()));
        }
        if(methodAnnotation != null){
            useDefault = methodAnnotation.useDefault();
            //if class annotation is disabled all methods are disabled as well
            if(classAnnotation == null){
                disabled = methodAnnotation.disable();
            }
            requiredPermissions.addAll(asList(methodAnnotation.requires()));
        }

        if(disabled) return;

        if (useDefault) {
            switch (requestContext.getMethod().toLowerCase()) {
                case "post":
                    requiredPermissions.add(CREATE);
                    break;
                case "put":
                    requiredPermissions.add(UPDATE);
                    break;
                case "delete":
                    requiredPermissions.add(DELETE);
                    break;
                case "get":
                    requiredPermissions.add(READ);
                    break;
                default:
            }
        }




        // no permission is required - perhaps because useDefault = false in Secured annotation
        // but user is authenticated so we let him pass.
        if (requiredPermissions.size() == 0) return;

        UriInfo uriInfo = requestContext.getUriInfo();
        String resource = requestContext.getUriInfo().getPath();
        String resourceRegex = resource;
        MultivaluedMap<String, String> params = uriInfo.getPathParameters();
        for(String key : params.keySet()){
            String param = params.getFirst(key);
            //not sure if it happens constantly -
            //previously the path would contain the PathParam key fx {id} but it seems now it actually contains the actual value
            //thus this check
            if(resourceRegex.contains("{" + key + "}")) {
                //if the path contains the key we replace it with the value and wildcard - assume only one
                resourceRegex = resourceRegex.replace("{" + key + "}", "\\*|(" + param + ")");
            }else{
                //if the parameter is somewhere in the middle between two slashes replace it
                if(resourceRegex.contains("/" + param + "/")) {
                    resourceRegex = resourceRegex.replaceAll("/" + param + "/", "/\\*|(" + param + ")/");
                }else{
                    //if the param is at the end
                    resourceRegex = resourceRegex.replaceAll("/" + param + "$", "/\\*|(" + param + ")");
                }
            }
        }

        resourceRegex = "^" + resourceRegex.replaceAll("/", "\\/") + "$";
        try {
            if(hasAccess(resourceRegex, user.getRolesFormatted(), requiredPermissions)){

            }
        }catch (RestException e) {
            error(requestContext, e.getStatus(), e.getMessage());
        }
    }

    /**
     * the result can be achieved directly via mongo with the following query
     *
     * <blockquote><pre>
     * db.Policy.aggregate([
     * {
     * 	$match: {
     *      {resources: {$regex: %resourceRegex% }},
     *      {roleId: {$in: %roles% }},
     *  }
     * },
     * {$unwind: "$permissions"},
     * {$group: {
     *      _id: "permissions_for_all_matching_policies",
     *      permissions: {$addToSet: "$permissions"}}},
     * {
     * 	$match: {
     * 		permissions: {$all: %permissions% }
     *    }
     * }])
     * </pre></blockquote>
     * @param resourceRegex regular expression matching required resources in mongoDB
     * @param roles list of the users roleId's to filter additional policies
     * @param permissions the required permissions on a resource
     * @return true if any policies matches the criteria
     */
    protected abstract boolean hasAccess(String resourceRegex,
                                         Set<String> roles,
                                         Set<String> permissions) throws RestException;

    private void error(ContainerRequestContext requestContext, Response.StatusType status, String message) {
        requestContext.abortWith(Response.status(status).entity(new RestError(message, status.getStatusCode())).build());
    }

    public abstract U getUser(ContainerRequestContext requestContext) throws RestException;

}
