package dk.agenia.permissionmanagement.annotations;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 16-05-2018
 * author: Runi
 * <p>
 * Intended for use with endpoint that create new resources.
 * This will create or update a policy for the current user for the newly created resource.
 * </p>
 * <p>
 * Example: given an endpoint with
 * path '/rest/v1/document' and returning an instance of Document.
 * When the request has successfully been processed the implementation of BaseOwnerAccessFilter will
 * give the current user READ and UPDATE permissions for the endpoint '/rest/v1/document/%document_id%'.
 * </p>
 * <p>
 * Per default the @targetResource is appended to the path of the annotated method which default is {id}.
 * If the path to the created resource is different set @appendTargetResource to false and set the @targetResource to the full path
 * </p>
 * <h1>IMPORTANT:</h1>
 * <ul>
 * <li>Make sure that you have a implementation of {@link dk.agenia.permissionmanagement.filters.BaseOwnerAccessFilter}
 * and that the filter is registered
 * </li>
 * </ul>
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface OwnerAccess {
    /**
     * per default uses the @Path value of the method (or class if the method is not annotated)
     */
    String targetResource() default "";

    /**
     * This value is appended to the @targetResource.
     * Which can be used to help to locate the target resource in the resource tree.
     */
    String parameterValue() default "{id}";

    /**
     * <p>
     * The method to call to get the id of the created element.
     * </p>
     * <p>
     * Note: the method should not take any parameters and the name is without any parenthesis.
     * </p>
     */
    String idMethod() default "getId";

    /**
     * <p>
     * The permissions to grant for the created element
     * </p>
     */
    String[] permissions() default {"READ", "UPDATE"};
}
