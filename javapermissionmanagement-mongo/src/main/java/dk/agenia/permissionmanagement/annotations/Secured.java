package dk.agenia.permissionmanagement.annotations;

import dk.agenia.permissionmanagement.models.Permission;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 20-04-2018
 * <p>Author: Runi</p>
 * <p>This annotation is used by the {@link dk.agenia.permissionmanagement.filters.BaseAuthenticationFilter}
 * to determine if a user has access to the requested endpoint.
 * </p>
 * Use this annotation along with Jersey rest annotations:
 * {@link javax.ws.rs.GET}, {@link javax.ws.rs.PUT}, {@link javax.ws.rs.POST} and {@link javax.ws.rs.DELETE}
 * When used the default behaviour will require the following permissions for each method:
 *
 * <ul>
 * <li>READ for GET method</li>
 * <li>CREATE for POST method</li>
 * <li>UPDATE for PUT method and</li>
 * <li>DELETE for DELETE method.</li>
 * </ul>
 *
 * To add additional permissions for an endpoint add a list of permission names in the requires parameter.
 * to override default behaviour listed above set the useDefault parameter to false and list any required permissions in the @Secured.requires field.
 * To disable authentication set @Secured.disable to true.
 * If @Secured.useDefault is set to false and @Secured.requires is empty no permissions will be required but a valid user still has to.
 *
 * <p><strong>note:</strong> if annotation is set on class and method the @Secured.requires is merged.</p>
 * <p>Also the @Secured.disable on the class overrules the one on method and @Secured.useDefault on method overrules the one on class</p>
 *
 */

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Secured {
    String[] requires() default {};
    boolean useDefault() default true;
    boolean disable() default false;
}
