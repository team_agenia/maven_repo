package dk.agenia.permissionmanagement.utils;

import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.controllers.ResourceController;
import dk.agenia.permissionmanagement.models.Permission;

import javax.ws.rs.Path;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created: 21-09-2018
 * author: Runi
 */

public class PermissionHelper {
    private static String[] restEndpointAnnotations = new String[]{
            "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"
    };

    public static Set<String> getActivePermissions(Class<?> aClass, String resource, ResourceController resourceController) {

        Secured securedAnnotation = getSecuredAnnotation(aClass);
        Path pathAnnotation = aClass.getAnnotation(Path.class);

        boolean classUseDefault = false; //not necessarily secured thus per default do not use default permissions

        Set<String> activePermissions = new HashSet<>();
        if (securedAnnotation != null) {
            classUseDefault = securedAnnotation.useDefault();
            activePermissions.addAll(Arrays.asList(securedAnnotation.requires()));
        }

        String classResource = trimSlashes(pathAnnotation.value());

        for (Method method : getMethods(aClass)) {
            boolean methodUseDefault = classUseDefault;
            if (isMethodEndpoint(method)) {
                //compute the name of the resource
                String methodResource = classResource;
                Path methodPathAnnotation = method.getAnnotation(Path.class);
                if (methodPathAnnotation != null) {
                    methodResource = resourceController.generateResource(classResource, trimSlashes(methodPathAnnotation.value()));
                }

                if (methodResource.equals(resource)) {
                    //compute which permissions are available for this resource

                    Secured methodSecuredAnnotation = method.getAnnotation(Secured.class);
                    if (methodSecuredAnnotation != null) {
                        methodUseDefault = methodSecuredAnnotation.useDefault();
                        activePermissions.addAll(Arrays.asList(methodSecuredAnnotation.requires()));
                    }

                    if (methodUseDefault) {
                        String defaultPermission = getDefaultPermission(method);
                        if (defaultPermission != null) activePermissions.add(defaultPermission);
                    }
                }
            }
        }

        return activePermissions;
    }

    public static boolean isMethodEndpoint(Method method) {
        for (Annotation annotation : method.getAnnotations()) {
            String name = annotation.annotationType().getSimpleName();
            for (String endpoint : restEndpointAnnotations) {
                if (endpoint.equals(name)) return true;
            }
        }
        return false;
    }

    public static List<Method> getMethods(Class<?> aClass) {
        List<Method> methods = new ArrayList<>();
        if (aClass == null) return methods;
        methods.addAll(Arrays.asList(aClass.getMethods()));
        methods.addAll(getMethods(aClass.getSuperclass()));
        return methods;
    }

    public static Secured getSecuredAnnotation(Class<?> aClass) {
        if (aClass == null) return null;
        Secured annotation = aClass.getAnnotation(Secured.class);
        if (annotation != null) return annotation;
        return getSecuredAnnotation(aClass.getSuperclass());
    }

    public static String getDefaultPermission(Method method) {
        for (Annotation annotation : method.getAnnotations()) {
            String name = annotation.annotationType().getSimpleName();
            switch (name) {
                case "POST":
                    return Permission.CREATE;
                case "PUT":
                    return Permission.UPDATE;
                case "DELETE":
                    return Permission.DELETE;
                case "GET":
                    return Permission.READ;
                default:
            }
        }
        return null;
    }

    public static String trimSlashes(String pathAnnotation) {
        if (pathAnnotation.startsWith("/")) pathAnnotation = pathAnnotation.substring(1);
        if (pathAnnotation.endsWith("/")) pathAnnotation = pathAnnotation.substring(0, pathAnnotation.length() - 1);
        return pathAnnotation;
    }
}
