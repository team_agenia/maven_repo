package dk.agenia.permissionmanagement.annotations;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 20-04-2018
 * <p>Author: Runi</p>
 * <p>This annotation is used by the {@link dk.agenia.permissionmanagement.filters.BaseAuthenticationFilter}
 * to determine if a user has access to the requested endpoint.
 * </p>
 * Use this annotation along with Jersey rest annotations:
 * {@link javax.ws.rs.GET}, {@link javax.ws.rs.PUT}, {@link javax.ws.rs.POST} and {@link javax.ws.rs.DELETE}
 * When used the default behaviour will require the following permissions for each method:
 *
 * <ul>
 * <li>READ for GET method</li>
 * <li>CREATE for POST method</li>
 * <li>UPDATE for PUT method and</li>
 * <li>DELETE for DELETE method.</li>
 * </ul>
 *
 * To add additional permissions for an endpoint add a list of permission names in the requires parameter.
 * to override default behaviour listed above set the useDefault parameter to false.
 *
 */

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Secured {
    String[] requires() default {};
    boolean useDefault() default true;
}
