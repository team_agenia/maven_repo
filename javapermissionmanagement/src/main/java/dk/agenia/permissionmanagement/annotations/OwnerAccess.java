package dk.agenia.permissionmanagement.annotations;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 16-05-2018
 * author: Runi
 * <p>
 * Intended for use with endpoint that create new resources.
 * This will create or update a policy for the current user for the newly created resource
 * </p>
 * <p>
 *     Example: given an endpoint with
 *     path '/rest/v1/document' and returning an instance of Document.
 *     When the request has successfully been processed the implementation of BaseOwnerAccessFilter will
 *     give the current user READ and UPDATE permissions for the endpoint '/rest/v1/document/{id}'.
 * </p>
 * <h1>IMPORTANT:</h1>
*  <ul>
*         <li>Make sure that you have a implementation of {@link dk.agenia.permissionmanagement.filters.BaseOwnerAccessFilter}
 *         and that the filter is registered
 *         </li>
*     </ul>
 *
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface OwnerAccess {
    String parentEndpoint() default "{id}";
    String[] permissions() default {"READ", "UPDATE"};
}
