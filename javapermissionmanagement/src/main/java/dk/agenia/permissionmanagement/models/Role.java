package dk.agenia.permissionmanagement.models;

import dk.agenia.permissionmanagement.util.PermissionUtil;
import dk.agenia.permissionmanagement.util.PolicyUtil;
import lombok.*;

import java.util.*;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Role extends BaseModel implements WithPermissions {


    private String name;
    private Set<Policy> policies;
    //many can inherit from the same one
    private Role parent;
    private List<Role> children; //TODO not necessary? we only go up in the hierarchy - but maybe useful for lookup

    public List<Policy> getAllPolicies(){
        if(this.policies == null) this.policies = new HashSet<>();
        List<Policy> policies = new ArrayList<>(this.policies);
        if(parent != null){
            policies.addAll(parent.getAllPolicies());
        }
        return policies;
    }

    @Override
    public Policy getPolicyByResource(String resource) {
        return PolicyUtil.getPolicyByResource(resource, policies);
    }

    @Override
    public boolean hasPermission(String resource, Permission permission) {
        return PermissionUtil.hasPermission(this, resource, permission)
                || (parent != null && parent.hasPermission(resource, permission));

    }

}
