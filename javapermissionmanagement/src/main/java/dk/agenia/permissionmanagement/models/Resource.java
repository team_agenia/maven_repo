package dk.agenia.permissionmanagement.models;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created: 26-04-2018
 * author: Runi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Resource extends BaseModel {

    private String name;
    @Builder.Default
    private Set<Resource> subResources = new HashSet<>();
    @Builder.Default
    private Set<String> activePermissions = new HashSet<>();

    public boolean isSecured() {
        return activePermissions != null && activePermissions.size() > 0;
    }

    public Set<String> getAvailablePermissions(){
        Set<String> set = new HashSet<>(activePermissions);
        for(Resource resource : subResources){
            set.addAll(resource.getAvailablePermissions());
        }
        return set;
    }
}
