package dk.agenia.permissionmanagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dk.agenia.permissionmanagement.util.PermissionUtil;
import dk.agenia.permissionmanagement.util.PolicyUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Iterator;
import java.util.Set;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PolicyUser extends BaseModel implements WithPermissions {

    private Set<Role> roles;
    private Set<Policy> policies;
    private String username;
    @JsonIgnore
    private String hash;

    public PolicyUser claims() {
        UserClaims claims = new UserClaims();
        claims.setUsername(username);
        claims.setId(getId());
        return claims;
    }

    public boolean hasPermission(String resource, Permission permission) {
        return PermissionUtil.hasPermission(this, resource, permission)
                || hasRolePermission(resource, permission, roles.iterator());
    }

    private boolean hasRolePermission(String resource, Permission permission, Iterator<Role> iterator) {
        if (iterator.hasNext())
            return iterator.next().hasPermission(resource, permission)
                    || hasRolePermission(resource, permission, iterator);
        return false;
    }

    @Override
    public Policy getPolicyByResource(String resource) {
       return PolicyUtil.getPolicyByResource(resource, policies);
    }
}
