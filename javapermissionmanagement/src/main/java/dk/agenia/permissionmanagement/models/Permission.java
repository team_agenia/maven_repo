package dk.agenia.permissionmanagement.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

/**
 * Created: 14-04-2018
 * Owner: Runi
 * <p>
 * Glorified extensible enum -
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class Permission extends BaseModel {

    private String name;
    //if custom permissions - this can be used to combine all permissions in one.
    private boolean isAdmin;

    public static final String DELETE = "DELETE";
    public static final String CREATE = "CREATE";
    public static final String UPDATE = "UPDATE";
    public static final String READ = "READ";
    public static final String ADMIN = "ADMIN";

    private static Permission[] defaultpermissions = new Permission[]{
            new Permission("00000000-0000-0000-0000-000000000001", CREATE),
            new Permission("00000000-0000-0000-0000-000000000002", UPDATE),
            new Permission("00000000-0000-0000-0000-000000000003", DELETE),
            new Permission("00000000-0000-0000-0000-000000000004", READ),
            new Permission("00000000-0000-0000-0000-000000000005", ADMIN, true)
    };

    public static Permission create() {
       return get(CREATE);
    }
    public static Permission update() {
       return get(UPDATE);
    }
    public static Permission delete() {
       return get(DELETE);
    }
    public static Permission read() {
       return get(READ);
    }
    public static Permission admin() {
       return get(ADMIN);
    }


    private static Permission get(String name){
        for (Permission p :
                defaultpermissions) {
            if (p.name.equals(name)) return p;
        }
        return null;
    }

    public Permission() {

    }

    public Permission(String name) {
        this(UUID.randomUUID().toString(), name);
    }

    public Permission(String id, String name) {
        super(id);
        this.name = name;
    }

    private Permission(String id, String name, boolean isAdmin){
        this(id, name);
        this.isAdmin = isAdmin;
    }

    public static Permission[] getDefault() {
        return defaultpermissions;
    }
}
