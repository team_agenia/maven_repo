package dk.agenia.permissionmanagement.models;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created: 20-04-2018
 * author: Runi
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Policy extends BaseModel {

    private Resource resource;
    //permission id, allowed
    @Builder.Default
    private Map<String, Boolean> allowed = new HashMap<>();

    private boolean inheritsFromParent;

    public boolean isAllowed(Permission permission) {
        if(permission == null) return false;
        if (allowed == null) return false;
        Boolean val = allowed.get(permission.getId());
        return val == null ? false : val;
    }

    /**
     * explicitly allow the given permission access for this policy
     *
     * @param permission
     */
    public void allow(Permission permission) {
        if(permission == null) return;
        if (allowed == null) allowed = new HashMap<>();
        allowed.put(permission.getId(), true);
    }

    /**
     * explicitly deny access for the given permission for this policy - this means if a parent policy grants access this will overwrite that
     *
     * @param permission
     */
    public void deny(Permission permission) {
        if(permission == null) return;
        if (allowed == null) allowed = new HashMap<>();
        allowed.put(permission.getId(), false);
    }

    /**
     * removes the given permission for this policy
     *
     * @param permission
     */
    public void remove(Permission permission) {
        if(permission == null) return;
        if (allowed == null) allowed = new HashMap<>();
        allowed.remove(permission.getId());
    }


}
