package dk.agenia.permissionmanagement.models;

public interface WithPermissions {

    Policy getPolicyByResource(String resource);
    boolean hasPermission(String resource, Permission permission);
}
