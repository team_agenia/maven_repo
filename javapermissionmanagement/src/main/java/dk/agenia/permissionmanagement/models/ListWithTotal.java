package dk.agenia.permissionmanagement.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 15-09-2018
 * author: Runi
 */
@Data
@Builder @AllArgsConstructor
@NoArgsConstructor
public class ListWithTotal<T> {
    @Builder.Default
    private List<T> list = new ArrayList<>();
    private Long count;
}
