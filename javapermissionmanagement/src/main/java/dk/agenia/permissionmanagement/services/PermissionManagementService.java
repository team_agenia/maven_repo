package dk.agenia.permissionmanagement.services;

import dk.agenia.permissionmanagement.models.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created: 26-04-2018
 * author: Runi
 */
/*
@Secured(requires = Permission.ADMIN)*/
@Path("management")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class PermissionManagementService<U extends PolicyUser, P extends Policy, S extends Resource, R extends Role> {

    @Context
    Application application;

    @GET
    @Path("resources")
    public abstract Set<S> getResources(@QueryParam("update") String update) throws Exception;

    @GET
    @Path("permissions")
    public List<Permission> getPermissions() throws Exception {
        return Arrays.asList(Permission.getDefault());
    }

    @GET
    @Path("roles")
    public abstract List<R> getRoles() throws Exception;

    @POST
    @Path("roles")
    public abstract R createRole(R role) throws Exception;

    @GET
    @Path("roles/{id}")
    public abstract R getRole(@PathParam("id") String id) throws Exception;

    @PUT
    @Path("roles/{id}")
    public abstract R updateRole(@PathParam("id") String id, R role) throws Exception;

    @DELETE
    @Path("roles/{id}")
    public abstract void deleteRole(@PathParam("id") String id, R role) throws Exception;

    @POST
    @Path("roles/{id}/policies")
    public abstract P createPolicyForRole(@PathParam("id") String roleId, P policy) throws Exception;

    @GET
    @Path("users")
    public abstract ListWithTotal<U> getUsers(@QueryParam("query") String query,
                                              @QueryParam("page") @DefaultValue("0") int page,
                                              @QueryParam("size") @DefaultValue("-1") int size,
                                              @QueryParam("orderBy") @DefaultValue("username") String orderBy,
                                              @QueryParam("order") @DefaultValue("asc") String order) throws Exception;

    @POST
    @Path("users")
    public abstract U createUser(U user) throws Exception;

    @GET
    @Path("users/{id}")
    public abstract U getUser(@PathParam("id") String id) throws Exception;

    @PUT
    @Path("users/{id}")
    public abstract U updateUser(@PathParam("id") String id, U user) throws Exception;

    @DELETE
    @Path("users/{id}")
    public abstract void deleteUser(@PathParam("id") String id, U user) throws Exception;


    @POST
    @Path("users/{id}/policies")
    public abstract P createPolicyForUser(@PathParam("id") String userId, P policy) throws Exception;

    @GET
    @Path("policies")
    public abstract List<P> getPolicies() throws Exception;

    @PUT
    @Path("policies/{id}")
    public abstract P updatePolicy(@PathParam("id") String id, P policy) throws Exception;

    @DELETE
    @Path("policies/{id}")
    public abstract void deletePolicy(@PathParam("id") String id, P policy) throws Exception;

}
