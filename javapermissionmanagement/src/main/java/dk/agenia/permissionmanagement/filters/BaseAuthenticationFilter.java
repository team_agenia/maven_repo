package dk.agenia.permissionmanagement.filters;

import dk.agenia.permissionmanagement.RestError;
import dk.agenia.permissionmanagement.RestException;
import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.models.*;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

import static dk.agenia.permissionmanagement.models.Permission.*;
import static dk.agenia.configservice.Configuration.get;

/**
 * Created: 14-04-2018
 * Owner: Runi
 */
@Slf4j
public abstract class BaseAuthenticationFilter<U extends PolicyUser> implements ContainerRequestFilter {

    private final String USER_CONTEXT_KEY = get("CONTEXT_USER_KEY", "user");


    @Context
    ResourceInfo resourceInfo;
    @Context
    HttpHeaders headers;


    public void filter(ContainerRequestContext requestContext) throws IOException {
        //options requests are not filtered
        if ("options".equals(requestContext.getMethod().toLowerCase())) return;

        U user;
        try {
            user = getUser(requestContext);
        } catch (RestException e) {
            error(requestContext, e.getStatus(), e.getMessage());
            return;
        }
        processRequest(requestContext, user);
        if (user != null)
            requestContext.setProperty(USER_CONTEXT_KEY, user);
    }

    public void processRequest(ContainerRequestContext requestContext, U user) throws IOException {


        Secured annotation = resourceInfo.getResourceMethod().getAnnotation(Secured.class);
        if (annotation == null) annotation = resourceInfo.getResourceClass().getAnnotation(Secured.class);
        if (annotation == null) return;

        Set<String> requiredPermissionNames = new HashSet<>(Arrays.asList(annotation.requires()));
        if (annotation.useDefault()) {
            switch (requestContext.getMethod().toLowerCase()) {
                case "post":
                    requiredPermissionNames.add(CREATE);
                    break;
                case "put":
                    requiredPermissionNames.add(UPDATE);
                    break;
                case "delete":
                    requiredPermissionNames.add(DELETE);
                    break;
                case "get":
                    requiredPermissionNames.add(READ);
                    break;
                default:
            }
        }

        List<String> resources = getResources(requestContext);

        if (requiredPermissionNames.size() > 0 && user == null) {
            error(requestContext, Response.Status.FORBIDDEN, "no user provided");
            return;
        }

        // no permission is required - perhaps because useDefault = false in Secured annotation
        if (requiredPermissionNames.size() == 0) return;

        List<Permission> permissions = getAllPermissions();
        Set<Permission> requiredPermissions = new HashSet<>();
        //compute all the required permissions
        for (String name : requiredPermissionNames) {
            for (Permission p : permissions) {
                if (p.getName().equals(name)) {
                    requiredPermissions.add(p);
                    break;
                }
            }
        }
        try {
            hasAccess(resources, user, requiredPermissions);
        } catch (RestException e) {
            error(requestContext, e.getStatus(), e.getMessage());
            return;
        }


    }

    private List<String> getResources(ContainerRequestContext requestContext) {

        List<String> matches = requestContext.getUriInfo().getMatchedURIs();
        MultivaluedMap<String, String> parameters = requestContext.getUriInfo().getPathParameters();
        List<String> resources = new ArrayList<>();
        for (String match : matches) {
            resources.add(match);
            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    String value = parameters.getFirst(key);
                    if (match.contains(value))
                        resources.add(match.replace(value, "{" + key + "}"));
                }
            }

        }
        return resources;
    }

    public boolean hasAccess(List<String> resources, U user, Set<Permission> requiredPermissions) throws RestException {
        Set<String> missingPermissions = new HashSet<>();
        Set<String> granted = new HashSet<>();

        List<Permission> allPermissions = getAllPermissions();
        Permission adminPermission = null;
        if (allPermissions != null) adminPermission = allPermissions.stream().filter(p -> p.isAdmin()).findFirst().get();

        //check for each resource if allowed. the first resource is the most specific resource
        for (String resource : resources) {

            //if there exists an admin permission which overrides all other permissions - we check if the user has this permission for this resource
            // if user has admin permission it is no longer necessary to check any other permissions
            if (adminPermission != null && user.hasPermission(resource, adminPermission)) {
                return true;
            }
            for (Permission required : requiredPermissions) {
                if (user.hasPermission(resource, required)) {
                    granted.add(required.getName());
                    //if previously identified as an missing permission we remove it from the missingPermissions set -
                    //this may occur when the permission is set on a parent resource and not the targeted resource
                    missingPermissions.remove(required.getName());
                } else {
                    missingPermissions.add(required.getName());
                }
                if (granted.size() >= requiredPermissions.size()) break;
            }
            if (granted.size() >= requiredPermissions.size()) break;
        }

        if (missingPermissions.size() > 0) {
            String val = "";
            for (String s : missingPermissions) val += s + ";";
            throw new RestException("Missing permission: " + val, Response.Status.FORBIDDEN);
        }

        return true;
    }

    private void error(ContainerRequestContext requestContext, Response.StatusType status, String message) {
        requestContext.abortWith(Response.status(status).entity(new RestError(message, status.getStatusCode())).build());
    }

    public abstract U getUser(ContainerRequestContext requestContext) throws RestException;

    public abstract List<Permission> getAllPermissions();

}
