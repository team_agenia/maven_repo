package dk.agenia.permissionmanagement.filters;

import dk.agenia.permissionmanagement.RestException;
import dk.agenia.permissionmanagement.annotations.OwnerAccess;
import dk.agenia.permissionmanagement.models.Permission;
import dk.agenia.permissionmanagement.models.PolicyUser;
import dk.agenia.permissionmanagement.util.ResourceUtil;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created: 16-05-2018
 * author: Runi
 */
@Slf4j
public abstract class BaseOwnerAccessFilter<T extends PolicyUser> implements ContainerResponseFilter {
    @Context
    ResourceInfo resourceInfo;
    @Context
    HttpHeaders headers;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        if(resourceInfo.getResourceMethod() == null) {
            log.error("Could not identify resource method");
            return;
        }
        OwnerAccess ownerAnnotation = resourceInfo.getResourceMethod().getAnnotation(OwnerAccess.class);
        if (ownerAnnotation == null) return;

        Path pathAnnotation = resourceInfo.getResourceMethod().getAnnotation(Path.class);
        if (pathAnnotation == null) pathAnnotation = resourceInfo.getResourceClass().getAnnotation(Path.class);

        String parentResource = pathAnnotation.value() + "/" + ownerAnnotation.parentEndpoint();

        Object entity = responseContext.getEntity();
        String id = getId(entity);
        if (id == null) return;

        T user = null;
        try {
            user = getUser(requestContext);
        } catch (RestException e) {
            log.error("could not get user", e);
        }

        String targetResource = pathAnnotation.value() + "/" + id;

        Set<String> activePermissions = ResourceUtil.getActivePermissions(resourceInfo.getResourceClass(), parentResource);
        Set<Permission> allowedPermissions = new HashSet<>();
        for (Permission p : getAllPermissions()) {
            for (String permissionName : ownerAnnotation.permissions()) {
                if (p.getName().equals(permissionName)) {
                    allowedPermissions.add(p);
                }
            }
        }
        try {
            grantAccess(user, parentResource, targetResource, activePermissions, allowedPermissions);
        } catch (Exception e) {
            log.error("Could not grant user access", e);
        }
    }


    public String getId(Object entity) {
        String id = null;
        try {
            Method method = entity.getClass().getMethod("getId", new Class[0]);
            Object idValue = method.invoke(entity, new Object[0]);
            id = idValue.toString();
        } catch (NoSuchMethodException e) {
            log.warn("id method not found", e);
        } catch (IllegalAccessException e) {
            log.warn("Illegal access on id method", e);
        } catch (InvocationTargetException e) {
            log.warn("id method invocation exception", e);
        }
        return id;
    }

    public abstract void grantAccess(
            T user,
            String parentResource,
            String targetResource,
            Set<String> activePermissions,
            Set<Permission> allowedPermissions) throws Exception;

    public abstract T getUser(ContainerRequestContext requestContext) throws RestException;

    public abstract List<Permission> getAllPermissions();
}