package dk.agenia.permissionmanagement.util;

import dk.agenia.permissionmanagement.models.Policy;

import java.util.Collection;

/**
 * Created: 15-05-2018
 * author: Runi
 */

public class PolicyUtil {
    public static Policy getPolicyByResource(String resource, Collection<Policy> policies) {
        for (Policy p : policies) {
            if (p.getResource().getName().equals(resource)) {
                return p;
            }
        }
        return null;
    }
}
