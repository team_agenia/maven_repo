package dk.agenia.permissionmanagement.util;

import dk.agenia.permissionmanagement.annotations.Secured;
import dk.agenia.permissionmanagement.models.Permission;
import dk.agenia.permissionmanagement.models.Resource;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Path;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created: 09-05-2018
 * author: Runi
 */
@Slf4j
public class ResourceUtil {

    private static String[] restEndpointAnnotations = new String[]{
            "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"
    };

    public static Set<Resource> getResources(Class<?>[] classes, String prefix, Collection<Permission> permissions) {
        return getResources(classes, prefix, permissions, null);
    }

    public static Set<Resource> getResources(Class<?>[] classes, String prefix, Collection<Permission> permissions, String targetResource) {

        Map<String, Resource> map = new HashMap<>();
        new HashSet<>(map.values());
        if (classes == null) return new HashSet<>();
        for (Class<?> aClass : classes) {
            //if class has annotation is not a subclass. Subclasses are fetched when processing their parent class
            if (isAnnotatedResourceClass(aClass) && !(prefix == null && aClass.isMemberClass())) {
                processResources(aClass, map, permissions);
            }
        }
        List<Resource> list = new ArrayList<>();
        for (Resource first : map.values()) {
            int longestMatch = 0;
            Resource longestMatchResource = null;
            for (Resource second : map.values()) {
                if (!first.getName().equals(second.getName())) {
                    if (first.getName().startsWith(second.getName())) { //second is parent
                        if (second.getName().length() > longestMatch) {
                            longestMatch = second.getName().length();
                            longestMatchResource = second;
                        }
                    }
                }
                //else it is the same resource
            }
            if (longestMatch == 0) { //no parent
                list.add(first);
            } else {
                longestMatchResource.getSubResources().add(first);
            }
        }
        return new HashSet<>(list);
    }

    private static Secured getSecuredAnnotation(Class<?> aClass) {
        if (aClass == null) return null;
        Secured annotation = aClass.getAnnotation(Secured.class);
        if (annotation != null) return annotation;
        return getSecuredAnnotation(aClass.getSuperclass());
    }

    private static void processResources(Class<?> methodClass, Map<String, Resource> resourceMap, Collection<Permission> permissions) {

        Secured securedAnnotation = getSecuredAnnotation(methodClass);
        Path pathAnnotation = methodClass.getAnnotation(Path.class);
        String path = pathAnnotation.value();
        path = trimSlashes(path);

        //String classResource = pathAnnotation.value();

        Resource currentResource = resourceMap.get(path);//Resource.builder().build();
//        if (parentResource != null) {
//            currentResource.setName(generateResource(parentResource.getName(), pathAnnotation.value()));
//        }else{
        if (currentResource == null) {
            currentResource = Resource.builder().name(path).build();
            resourceMap.put(currentResource.getName(), currentResource);
        }
//        }

        boolean classUseDefault = false; //not necessarily secured thus per default do not use default permissions
        Set<String> classActivePermissions = new HashSet<>();

        if (securedAnnotation != null) {
            classUseDefault = securedAnnotation.useDefault();
            classActivePermissions.addAll(Arrays.asList(securedAnnotation.requires()));
        }

        for (Method method : getMethods(methodClass)) {
            boolean methodUseDefault = classUseDefault;
            if (isMethodEndpoint(method)) {
                //compute the name of the resource
                String methodResource = currentResource.getName();
                Path methodPathAnnotation = method.getAnnotation(Path.class);
                if (methodPathAnnotation != null) {
                    methodResource = generateResource(currentResource.getName(), methodPathAnnotation.value());
                }

                //compute which permissions are available for this resource - add admin permissions per default
                Set<String> methodActivePermissions = permissions
                        .stream()
                        .filter(Permission::isAdmin)
                        .map(Permission::getName)
                        .distinct()
                        .collect(Collectors.toSet());
                Secured methodSecuredAnnotation = method.getAnnotation(Secured.class);
                if (methodSecuredAnnotation != null) {
                    methodUseDefault = methodSecuredAnnotation.useDefault();
                    methodActivePermissions.addAll(Arrays.asList(methodSecuredAnnotation.requires()));
                    if (methodUseDefault) //stack permissions if using default
                        methodActivePermissions.addAll(classActivePermissions);
                } else {
                    methodActivePermissions.addAll(classActivePermissions);
                }

                if (methodUseDefault) {
                    String defaultPermission = getDefaultPermission(method);
                    if (defaultPermission != null) methodActivePermissions.add(defaultPermission);
                }

//                if (parentResource != null) {
                //TODO incomment to support nested classes - this needs more work - look for similar comment that is for same functionality
                    /*String finalMethodResource = methodResource;
                    //TODO it should probably not be possible to have two innerclasses with same path? or is there another scenario where resource might already exist?
                    Optional<Resource> existing = parentResource.getSubResources().stream().filter(r -> r.getName().equals(finalMethodResource)).findFirst();
                    if (existing.isPresent()) {
                        existing.get().getActivePermissions().addAll(methodActivePermissions);
                    } else {
                        parentResource.getSubResources().add(
                                Resource.builder()
                                        .name(methodResource)
                                        .activePermissions(methodActivePermissions)
                                        .build());
                    }*/
//                } else {
                //if subresource add it to parent
                if (!methodResource.equals(currentResource.getName())) {
                    String finalMethodResource1 = methodResource;
//                    Optional<Resource> existing = currentResource.getSubResources().stream().filter(r -> r.getName().equals(finalMethodResource1)).findFirst();
                    Resource existing = resourceMap.get(methodResource);
                    if (existing != null) {
                        existing.getActivePermissions().addAll(methodActivePermissions);
                    } else {
//                        currentResource.getSubResources().add(Resource.builder().name(methodResource).activePermissions(methodActivePermissions).build());
                        resourceMap.put(methodResource,
                                Resource.builder()
                                        .name(methodResource)
                                        .activePermissions(methodActivePermissions)
                                        .build());
                    }
                } else {
                    //methodResource == classResource
                    currentResource.getActivePermissions().addAll(methodActivePermissions);
                }
//                }
            }
        }

        //TODO incomment to support nested classes - this needs more work - look for similar comment that is for same functionality
        /*for (Class<?> aClass : methodClass.getClasses()) {
            processResources(aClass, resourceMap, currentResource);
        }*/

    }

    private static String trimSlashes(String pathAnnotation) {
        if (pathAnnotation.startsWith("/")) pathAnnotation = pathAnnotation.substring(1);
        if (pathAnnotation.endsWith("/")) pathAnnotation = pathAnnotation.substring(0, pathAnnotation.length() - 1);
        return pathAnnotation;
    }

    private static List<Method> getMethods(Class<?> aClass) {
        List<Method> methods = new ArrayList<>();
        if (aClass == null) return methods;
        methods.addAll(Arrays.asList(aClass.getMethods()));
        methods.addAll(getMethods(aClass.getSuperclass()));
        return methods;
    }

    public static Set<String> getActivePermissions(Class<?> aClass, String resource) {

        Secured securedAnnotation = getSecuredAnnotation(aClass);
        Path pathAnnotation = aClass.getAnnotation(Path.class);

        boolean classUseDefault = false; //not necessarily secured thus per default do not use default permissions

        Set<String> activePermissions = new HashSet<>();
        if (securedAnnotation != null) {
            classUseDefault = securedAnnotation.useDefault();
            activePermissions.addAll(Arrays.asList(securedAnnotation.requires()));
        }

        String classResource = trimSlashes(pathAnnotation.value());

        for (Method method : getMethods(aClass)) {
            boolean methodUseDefault = classUseDefault;
            if (isMethodEndpoint(method)) {
                //compute the name of the resource
                String methodResource = classResource;
                Path methodPathAnnotation = method.getAnnotation(Path.class);
                if (methodPathAnnotation != null) {
                    methodResource = generateResource(classResource, trimSlashes(methodPathAnnotation.value()));
                }

                if (methodResource.equals(resource)) {
                    //compute which permissions are available for this resource

                    Secured methodSecuredAnnotation = method.getAnnotation(Secured.class);
                    if (methodSecuredAnnotation != null) {
                        methodUseDefault = methodSecuredAnnotation.useDefault();
                        activePermissions.addAll(Arrays.asList(methodSecuredAnnotation.requires()));
                    }

                    if (methodUseDefault) {
                        String defaultPermission = getDefaultPermission(method);
                        if (defaultPermission != null) activePermissions.add(defaultPermission);
                    }
                }
            }
        }

        return activePermissions;
    }

    public static String getDefaultPermission(Method method) {
        for (Annotation annotation : method.getAnnotations()) {
            String name = annotation.annotationType().getSimpleName();
            switch (name) {
                case "POST":
                    return Permission.CREATE;
                case "PUT":
                    return Permission.UPDATE;
                case "DELETE":
                    return Permission.DELETE;
                case "GET":
                    return Permission.READ;
                default:
                    log.info("Annotation is not relevant");//log.info("Method is not a rest endpoint that we are interested in");
            }
        }
        return null;
    }

    public static String generateResource(String prefix, String resource) {
        if (prefix == null) return resource;
        if (prefix.endsWith("/")) prefix = prefix.substring(0, prefix.length() - 1);
        if (!resource.startsWith("/")) resource = "/" + resource;
        return prefix + resource;
    }

    private static boolean isMethodEndpoint(Method method) {
        for (Annotation annotation : method.getAnnotations()) {
            String name = annotation.annotationType().getSimpleName();
            for (String endpoint : restEndpointAnnotations) {
                if (endpoint.equals(name)) return true;
            }
        }
        return false;
    }

    private static boolean isAnnotatedResourceClass(Class rc) {
        if (rc.isAnnotationPresent(Path.class)) {
            return true;
        }

        for (Class i : rc.getInterfaces()) {
            if (i.isAnnotationPresent(Path.class)) {
                return true;
            }
        }
        return false;
    }

}
