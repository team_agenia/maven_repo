package dk.agenia.permissionmanagement.util;

import dk.agenia.permissionmanagement.models.Permission;
import dk.agenia.permissionmanagement.models.Policy;
import dk.agenia.permissionmanagement.models.WithPermissions;

/**
 * Created: 15-05-2018
 * author: Runi
 */

public class PermissionUtil {

    public static boolean hasPermission(WithPermissions entity, String resource, Permission permission) {
        Policy p = entity.getPolicyByResource(resource);
        if(p == null) return false;
        return p.isAllowed(permission);
    }


}
