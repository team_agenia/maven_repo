import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created: 18-04-2018
 * Owner: Runi
 */

public class Test {

    public static void main(String[] args) {
        List<String> strippedSegments = new ArrayList<>(Arrays.asList("1"));

        int indexHigh = strippedSegments.size()-1;

        for(int indexLow = 0; indexLow < indexHigh; indexLow++){
            String segmentLow = strippedSegments.get(indexLow);
            String segmentHigh = strippedSegments.get(indexHigh);
            strippedSegments.set(indexHigh--, segmentLow);
            strippedSegments.set(indexLow++, segmentHigh);
        }

        for(String s : strippedSegments) System.out.println(s);

    }
}
