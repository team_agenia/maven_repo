import dk.agenia.permissionmanagement.RestException;
import dk.agenia.permissionmanagement.filters.BaseAuthenticationFilter;
import dk.agenia.permissionmanagement.models.Permission;

import javax.ws.rs.container.ContainerRequestContext;
import java.util.List;

/**
 * Created: 06-06-2018
 * author: Runi
 */

public class AuthFilter extends BaseAuthenticationFilter<User> {
    @Override
    public User getUser(ContainerRequestContext requestContext) throws RestException {
        return null;
    }

    @Override
    public List<Permission> getAllPermissions() {
        return null;
    }
}
