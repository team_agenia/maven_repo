import React from 'react';
import ReactDOM from 'react-dom';
import './styles/css/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import ResourceStore from "./stores/ResourceStore";


export class TransportLayer {

    base = process.env.NODE_ENV === "development" ? "http://localhost:8380/rest/v1" : "/rest/v1";
    constructor(){

    }

    fetchResources(update = ""){
        return this.GET(`/management/resources?update=${update}`);
    }

    fetchPermissions(){
        return this.GET(`/management/permissions`);
    }

    fetchRoles(){
        return this.GET(`/management/roles`);
    }

    fetchUsers(){
        return this.GET('/management/users');
    }

    createRole(role){
        return this.POST('/management/roles', role);
    }

    createUser(user){
        return this.POST('/management/users', user);
    }

    createPolicy(roleId, policy){
        return this.POST(`/management/roles/${roleId}/policies`, policy);
    }

    updateRole(role){
        return this.PUT(`/management/roles/${role.id}`, role);
    }

    updateUser(user){
        return this.PUT(`/management/users/${user.id}`, user);
    }

    updatePolicy(id, policy){
        return this.PUT(`/management/policies/${id}`, policy);
    }

    deleteRole(id){
        return this.DELETE(`/management/roles/${id}`);
    }

    deleteUser(id){
        return this.DELETE(`/management/users/${id}`);
    }

    POST = (route, body) => {
        return this._fetch(route, body, 'POST');
    }

    PUT = (route, body) => {
        return this._fetch(route, body, 'PUT');
    }

    GET = (route) => {
        return this._fetch(route, null, 'GET');
    }

    DELETE = (route) => {
        return this._fetch(route, null, 'DELETE');
    }

    _fetch = (route, body, method) => {
        const json = JSON.stringify((body || {}));
        let content =  {
            method: method,
            cache: 'no-cache',
            mode: 'cors',
            redirect: 'follow',
            headers: {
                'content-type': 'application/json'
            }
        };
        if(body) content.body = json;
        return fetch(`${this.base}${route}`,content)
            .then(res => res.status === 204 ? {} : res.json());
    }

}


ReactDOM.render(<App store={new ResourceStore(new TransportLayer())}/>, document.getElementById('root'));
registerServiceWorker();