import React, {Component} from 'react';
import './App.css';
import ResourceStore from "./stores/ResourceStore";
import PropTypes from 'prop-types';
import {withStyles} from "material-ui/styles";
import red from 'material-ui/colors/red';
import {decorate, observable, runInAction} from "mobx";
import CreateRole from "./components/CreateRole";
import {observer} from "mobx-react";
import {AppBar, Button, Tab, Tabs, Toolbar, Typography} from "material-ui";
import CreateUser from "./components/CreateUser";
import EditUser from "./components/EditUser";
import Tree from "./components/treeview/Tree";
import EditRole from "./components/EditRole";
import TableOverview from "./components/TableOverview";

const styles = theme => ({
    root: {
        /* display: 'flex',
         justifyContent: 'center',
         alignItems: 'flex-end',*/
    },
    flex: {
        flex: 1,
    },
    icon: {
        margin: theme.spacing.unit * 2,
        color: red[500],
        '&:hover': {
            color: red[800]
        }
    },
    iconHover: {
        margin: theme.spacing.unit * 2,
        '&:hover': {
            color: red[800],
        },
    },
});

class App extends Component {

    doCreateRole = false;
    doCreateUser = false;

    selectedTab = 0;
    selectedUser = null;
    selectedRole = null;

    constructor(props) {
        super(props);
    }

    createRole = (role) => {
        this.props.store.createRole(role);
        this.doCreateRole = false;
    }

    saveRole = (role) => {
        this.props.store.updateRole(role).then(() => this.selectedRole = null);
    }

    deleteRole = (role) => {
        this.props.store.deleteRole(role).then(() => this.selectedRole = null);
    }

    deleteUser = (user) => {
        this.props.store.deleteUser(user).then(() => this.selectedUser = null);
    }

    createUser = (user) => {
        this.props.store.createUser(user);
        this.doCreateUser = false;
    }

    updateUser = (user) => {
        this.selectedUser = user;
    }

    saveUser = (user) => {
        this.props.store.updateUser(user);
        this.selectedUser = null;
    }

    selectTab = (event, tab) => {
        this.selectedTab = tab;
    }

    render() {
        const {roleTree, resources, permissions, roles, users, currentResource, currentRole, currentUser} = this.props.store;
        const {classes} = this.props;
        const {selectedTab} = this;
        return (
            <div className="App">
                {this.doCreateRole &&
                <CreateRole roles={roles} onCreate={this.createRole} onClose={() => this.doCreateRole = false}/>
                }
                {this.doCreateUser &&
                <CreateUser roles={roles} onCreate={this.createUser} onClose={() => this.doCreateUser = false}/>
                }
                {this.selectedUser &&
                <EditUser roles={roles}
                          user={this.selectedUser}
                          onClose={() => this.selectedUser = null}
                          onDelete={this.deleteUser}
                          onSave={this.saveUser}/>
                }
                {this.selectedRole &&
                <EditRole roles={roles} role={this.selectedRole}
                          onClose={() => this.selectedRole = null}
                          onDelete={this.deleteRole}
                          onSave={this.saveRole}/>
                }
                <div className={classes.root}>

                </div>

                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            <Tabs value={selectedTab} onChange={this.selectTab}>
                                <Tab label="Resources"/>
                                <Tab label="Roles"/>
                                <Tab label="Users"/>
                            </Tabs>
                        </Typography>
                        <Button color="inherit" onClick={this.props.store.updateResources}>Update resources</Button>
                        <Button color="inherit" onClick={() => this.doCreateUser = true}>Add user</Button>
                        <Button color="inherit" onClick={() => this.doCreateRole = true}>Add role</Button>
                    </Toolbar>
                </AppBar>

                <div className="permission-tabs">
                    {selectedTab === 0 &&
                    <div>
                        <Tree selected={currentResource}
                              title="Resources"
                              nodes={resources}
                              options={{
                                  childProp: "subResources",
                                  childElements: (resource) => resource.subResources,
                                  onSelect: resource => this.props.store.currentResource = resource
                              }}/>
                        <div style={{display: "inline-block"}}>
                            <TableOverview
                                title="Roles"
                                selected={this.props.store.currentResource}
                                permissions={permissions}
                                list={roleTree}
                                rowOptions={{
                                    elementSelectable: true,
                                    onEdit: (role) => this.selectedRole = role,
                                    onUpdatePermission: (role, resource, permission, checked) => {
                                        runInAction(() => {
                                            this.props.store.updatePermission(role, resource.name, permission, checked);
                                        })
                                    },
                                    hasPermission: (role, resource, permission) => {
                                        if (!role) return false;
                                        return role.hasPermission(resource, permission);
                                    },
                                    isPermissionAvailable: (role, resource, permission) => {
                                        if (!resource) return false;
                                        return resource.availablePermissions.find(a => a === permission.name);
                                    }
                                }}

                            />
                        </div>
                    </div>
                    }
                    {selectedTab === 1 &&
                    <div>
                        <Tree selected={currentRole}
                              title="Roles"
                              nodes={roleTree}
                              options={{
                                  childElements: role => role.children,
                                  onSelect: role => this.props.store.currentRole = role,
                                  editable: true,
                                  onEdit: role => this.selectedRole = role
                              }}/>
                        <div style={{display: "inline-block"}}>
                            <TableOverview
                                title="Resources"
                                selected={this.props.store.currentRole}
                                permissions={permissions}
                                list={resources}
                                rowOptions={{
                                    onEdit: (resource) => {
                                        debugger
                                    },
                                    onUpdatePermission: (resource, role, permission, checked) => {
                                        runInAction(() => {
                                            this.props.store.updatePermission(role, resource.name, permission, checked);
                                        })
                                    },
                                    hasPermission: (resource, role, permission) => {
                                        if (!role) return false;
                                        return role.hasPermission(resource, permission);
                                    },
                                    isPermissionAvailable: (resource, role, permission) => {
                                        if (!resource) return false;
                                        return resource.availablePermissions.find(a => a === permission.name);
                                    },
                                    childProp: "subResources"
                                }}/>
                        </div>
                    </div>
                    }
                    {selectedTab === 2 &&
                    <div>
                        <Tree selected={currentUser}
                              title="Users"
                              nodes={users}
                              options={{
                                  hasChildren: false,
                                  onSelect: user => this.props.store.currentUser = user,
                                  editable: true,
                                  onEdit: user => this.selectedUser = user
                              }}/>
                        <div style={{display: "inline-block"}}>
                            <TableOverview title="Resources"
                                           selected={this.props.store.currentUser}
                                           permissions={permissions}
                                           list={resources}
                                           rowOptions={{
                                               onEdit: (resource) => {
                                                   debugger
                                               },
                                               onUpdatePermission: (resource, user, permission, checked) => {
                                                   runInAction(() => {
                                                       this.props.store.updatePermissionForUser(user, resource.name, permission, checked);
                                                   })
                                               },
                                               hasPermission: (resource, user, permission) => {
                                                   if (!user) return false;
                                                   return user.hasPermission(resource, permission);
                                               },
                                               isPermissionAvailable: (resource, user, permission) => {
                                                   if (!resource) return false;
                                                   return resource.availablePermissions.find(a => a === permission.name);
                                               },
                                               childProp: "subResources"
                                           }}/>
                        </div>
                    </div>


                    }
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(observer(App));

App.propTypes = {
    store: PropTypes.instanceOf(ResourceStore),
    classes: PropTypes.object.isRequired
}

decorate(App, {
    selectedTab: observable,
    doCreateRole: observable,
    doCreateUser: observable,
    selectedUser: observable,
    selectedRole: observable
})
