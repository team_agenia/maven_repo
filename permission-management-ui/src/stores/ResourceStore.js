import {computed, decorate, observable, action} from 'mobx';
import Role from "../models/Role";
import Policy from "../models/Policy";
import Resource from "../models/Resource";
import User from "../models/User";

/*
created: 30-04-2018
created by: Runi
*/

export default class ResourceStore {

    currentResource = null;
    currentRole = null;
    currentUser = null;

    resources = [];
    permissions = [];
    roles = [];
    users = [];
    transportLayer;

    constructor(transportLayer) {
        this.transportLayer = transportLayer;

        this.transportLayer.fetchUsers().then(users => {
            this.users = users.map(user => new User(user, this));
            if (!this.currentUser) this.currentUser = this.users[0];
        })
        //testData.roles.map(role => Object.assign(role, {expanded: false}));
        this.transportLayer.fetchRoles().then(json => {
            let roles = json.map(role => new Role(role));
            roles.forEach(r => {
                if (r.parent) {
                    let parent = roles.find(p => p.id === r.parent.id);
                    parent.children.push(r);
                    r.parent = parent;
                }
            })
            this.roles = roles;
            if (!this.currentRole) this.currentRole = roles[0]; //set default value
        })

        this.transportLayer.fetchPermissions().then(json => {
            this.permissions = json;
        })
        this.transportLayer.fetchResources().then(json => this.processResources(json));
    }

    updateResources = () => {
        this.transportLayer.fetchResources("true").then(json => this.processResources(json));
    }

    processResources = (json) => {
        json = json.sort((a, b) => a.name > b.name);
        json.forEach(resource => {
            resource.subResources = resource.subResources
                .sort((a, b) => a.name > b.name);
        })
        this.resources = json.map(r => new Resource(r));
        //set default value
        if(!this.currentResource) this.currentResource = this.resources[0];
    }

    createRole = (role) => {
        this.transportLayer.createRole(role).then(created => {
            const n = new Role(created);
            if (n.parent) {
                let parent = this.roles.find(r => r.id === n.parent.id);
                parent.children.push(n);
                n.parent = parent;
            }
            this.roles.push(n);
        });
    }

    createUser = (user) => {
        this.transportLayer.createUser(user).then(created => {
            let u = new User(created, this);
            this.users.push(u);
            if (u.roles) {
                let existingRoles = [];
                u.roles.forEach(role => {
                    existingRoles.push(this.roles.find(e => e.id === role.id));
                });
                u.roles = existingRoles;
            }
        })
    }

    updateRole = (role) => {
        let existing = this.roles.find(r => r.id === role.id);

        let json = existing.json;

        json.name = role.name;
        json.parent = role.parent && role.parent.json;
        return this.transportLayer.updateRole(json).then(updated => {
            if (existing.parent) {
                existing.parent.children.splice(existing.parent.children.indexOf(existing), 1);
                existing.parent = null;
            }
            if (updated.parent) {
                let parent = this.roles.find(r => r.id === updated.parent.id);
                parent.children.push(existing);
                existing.parent = parent;
            }
            existing.name = updated.name;
        });
    }

    updateUser = (user) => {
        let existing = this.users.find(u => u.id === user.id);
        let json = existing.json;
        json.name = user.name;
        json.roles = user.roles.map(r => r.json);
        this.transportLayer.updateUser(json).then(updated => {
            existing.json = updated;
        })
    }

    updatePolicy = (role, resourceName, permissionId, isAllowed) => {
        let policy = role.policies.find(p => p.resource.name === resourceName);
        if (!policy) {
            let resource = this.resourcesFlat.find(r => r.name === resourceName);
            let allowed = {};
            allowed[permissionId] = isAllowed;
            this.transportLayer.createPolicy(role.id, {
                    resource: resource.json,
                    allowed: allowed
                }
            ).then(created => {
                role.policies.push(new Policy(created));
            })
        } else {
            policy.allowed.set(permissionId, isAllowed);
            this.transportLayer.updatePolicy(policy.id, policy.json);
        }
    }

    deleteRole = (role) => {
        return this.transportLayer.deleteRole(role.id).then(() => {
            if (role.parent) {
                role.parent.children.splice(role.parent.children.indexOf(role), 1);
                this.roles.splice(this.roles.indexOf(role), 1);
            }
        });
    }

    deleteUser = (user) => {
        return this.transportLayer.deleteUser(user.id).then(() => {
            this.users.splice(this.users.indexOf(user), 1);
            if (this.currentUser) {
                if (this.currentUser.id === user.id && this.users.length > 0) {
                    this.currentUser = this.users[0];
                }
            }
        })
    }

    get resourcesFlat() {
        let list = [];
        let iterator = (resource) => {
            if (!list.find(r => r.id === resource.id)) {
                list.push(resource);
            }
            if (resource.subResources) {
                resource.subResources.forEach(s => iterator(s));
            }
        }
        this.resources.forEach(r => iterator(r));
        return list;
    }

    get roleTree() {
        let root = this.roles.filter(r => !r.parent);

        return root;
    }


    //TODO deprecated
    /*get permissionValues() {
        if (!this.currentResource) return {};
        let values = {};
        this.roles.forEach(role => {
            values[role.id] = {};
            this.permissions.forEach(permission => {

                //force boolean
                values[role.id][permission.id] = role.hasPermission(this.currentResource, permission);
            });
        })
        return values;
    }*/


    //TODO deprecated
    updateChildrenInherit = (role, resourceName, childrenInherit) => {
        let policy = this.findPolicy(role,
            role.policies.find(p => p.resource.name === resourceName),
            resourceName);
        policy.childrenInherit = childrenInherit;
        debugger;
    }

    updatePermission = (role, resourceName, permission, allowed) => {
        this.updatePolicy(role, resourceName, permission.id, allowed);
    }

    updatePermissionForUser = (user, resourceName, permission, allowed) => {
        this.updatePolicy(user, resourceName, permission.id, allowed);
    }

    //TODO deprecated
    findPolicy = (role, policyId, resourceName) => {
        if (!role) return;
        let policy = role.policies.find(policy => policy.id === policyId);
        if (!policy) {
            policy = new Policy({
                id: policyId,
                resource: resourceName,
                permissions: [],
                allowed: {}
            });
            role.policies.push(policy);
        }
        return policy;
    }

}

decorate(ResourceStore, {
    currentResource: observable,
    currentRole: observable,
    currentUser: observable,
    /*permissionValues: computed,*/
    roleTree: computed,
    resources: observable,
    permissions: observable,
    roles: observable,
    users: observable,
    updatePermission: action,
    toggleRow: action,
    updatePolicy: action,
    processResources: action
});