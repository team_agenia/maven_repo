import {extendObservable, autorun, observer, reaction, decorate, observable} from 'mobx';
import Policy from "./Policy";

/*
created: 03-05-2018
created by: Runi
*/

export default class Role {

    id;
    name;
    policies;
    parent;
    children = [];

    constructor(json) {
        Object.assign(this, json);
        this.policies = (json.policies||[]).map(p => new Policy(p));
        this.children = [];
    }

    hasPermission = (resource, permission) => {
        if(!resource) return false;
        let policy = this.policies.find(p => p.resource.name === resource.name);
        return (policy && policy.allowed.get(permission.id) && this.name) || (this.parent && this.parent.hasPermission(resource, permission));
    }

    isParent = (role) => {
        return this.id === role.id || (this.parent && this.parent.isParent(role));
    }

    get json(){
        return {
            id: this.id,
            name: this.name,
            policies: this.policies.map(p => p.json),
            parent: this.parent && this.parent.json
        }
    }

    set json(json){
        this.name = json.name;
        this.id = json.id;
    }

}

decorate(Role, {
    name: observable,
    policies: observable,
    parent: observable,
    children: observable
})
