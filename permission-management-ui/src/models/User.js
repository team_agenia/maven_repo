import {computed, decorate, observable} from 'mobx';
import Policy from "./Policy";
import Role from "./Role";

/*
created: 14-05-2018
created by: Runi
*/

export default class User {

    id;
    name;
    roleIds = [];
    policies;

    store;

    constructor(json, store) {
        const {id, name, roles, policies} = json;
        this.id = id;
        this.name = name;
        this.store = store;

        this.policies = (json.policies || []).map(p => new Policy(p));
        this.roleIds = (roles || []).map(r => r.id);
    }

    hasPermission = (resource, permission) => {
        if(!resource) return false;
        let policy = this.policies.find(p => p.resource.name === resource.name);
        return (policy && policy.allowed.get(permission.id)) || this.hasRolePermission(resource, permission);
    };

    hasRolePermission = (resource, permission) => {
        let granted = false;
        this.roles.forEach(r => {
            if(r.hasPermission(resource, permission)){
                granted = r.name;
            }
        })
        return granted;
    }

    get roles(){
        return this.roleIds.map(id => this.store.roles.find(r => r.id === id) || {id: id});
    }

    set roles(roles){
        this.roleIds = roles.map(r => r.id);
    }

    get json(){
        return {
            id: this.id,
            roles: this.roles.map(r => r.json),
            name: this.name,
            policies: this.policies.map(p => p.json)
        }
    }

    set json(json){
        this.name = json.name;
        this.roleIds = (json.roles || []).map(r => r.id);
    }
}

decorate(User, {
    name: observable,
    roleIds: observable,
    policies: observable
})
