import {observable, decorate} from 'mobx';

/*
created: 12-05-2018
created by: Runi
*/

export default class Resource {

    id;
    name;
    subResources;
    activePermissions;
    secured;
    availablePermissions;

    constructor(json) {
        Object.assign(this, json);
        this.subResources = (json.subResources || []).map(r => new Resource(r));
    }

    get json(){
        return {
            id: this.id,
            name: this.name,
            subResources: this.subResources.map(r => r.json),
            activePermissions: this.activePermissions
        }
    }
}
