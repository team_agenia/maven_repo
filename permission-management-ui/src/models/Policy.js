import {decorate, observable} from 'mobx';
import Resource from "./Resource";

/*
created: 03-05-2018
created by: Runi
*/

export default class Policy {

    id;
    resource;
    childrenInherit;
    allowed;

    constructor(json) {
        Object.assign(this, json);
        this.resource = new Resource(json.resource);
        this.allowed = observable.map(json.allowed, {deep: true});
        this.childrenInherit = !!json.childrenInherit;
    }

    get json(){
        return{
            id: this.id,
            resource: this.resource.json,
            childrenInherit: this.childrenInherit,
            allowed: this.allowed
        }
    }

}

decorate(Policy, {
    resource: observable,
    childrenInherit: observable,
    allowed: observable
})
