import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import {decorate, observable, reaction} from "mobx";
import {Icon, IconButton} from "material-ui";

const iconSize = 24;

class TreeNode extends Component {

    expanded = false;
    hasChildren = true;

    constructor(props) {
        super(props);
        this.expanded = (props.node[props.childProp] || []).length > 0;
        this.hasChildren = props.hasChildren;
        reaction(() => this.expanded, (expanded) => {
            /*this.props.store.getElements(this.props.)*/
            if (this.props.childElements(this.props.node).length === 0) this.hasChildren = false;
        })
    }

    toggleExpand = () => {
        this.expanded = !this.expanded;
    }

    onSelect = () => {
        this.props.onSelect(this.props.node);
    }

    onEdit = () => {
        this.props.onEdit(this.props.node);
    }

    render() {
        const {nameProp, matchingProp, childElements, selected, node, margin, editable} = this.props;
        const active = selected && (selected[matchingProp] === node[matchingProp]);
        const marginSum = margin + (this.hasChildren ? 0 : iconSize);
        return (
            <div className={"tree-node"}>
                <div className={"content" + (active ? " active" : "")}>
                    <div style={{marginLeft: `${marginSum}px`}}>
                        {this.hasChildren &&
                        <IconButton variant="fab"
                                    size="small"
                                    aria-label="add"
                                    style={{display: "inline-block", width: `${iconSize}px`, height: `${iconSize}px`}}
                                    onClick={() => {
                                        this.toggleExpand()
                                    }}>
                            <Icon>{this.expanded ? "remove" : "add"}</Icon>
                        </IconButton>
                        }
                        <div className={"text"} style={{width: `calc(100% - ${15 + (this.hasChildren ? iconSize : 0)}px)`}}
                             onClick={this.onSelect}>
                            {node[nameProp]}
                            {editable && <span className="edit-link" onClick={this.onEdit}>edit</span>}
                        </div>
                    </div>
                </div>
                {this.expanded &&
                <div className="tree-node-children">
                    {childElements(node).map(child => <TreeNode {...this.props}
                                                                key={child[nameProp]}
                                                                margin={this.props.margin + 15}
                                                                node={child}/>)}
                </div>
                }
            </div>
        )
    }
}

export default observer(TreeNode)

TreeNode.propTypes = {
    node: PropTypes.object,
    margin: PropTypes.number,
    selected: PropTypes.object,
    matchingProp: PropTypes.string,
    childProp: PropTypes.string,
    nameProp: PropTypes.string,
    childElements: PropTypes.func,
    onSelect: PropTypes.func.isRequired,
    editable: PropTypes.bool,
    onEdit: PropTypes.func,
    hasChildren: PropTypes.bool
}

TreeNode.defaultProps = {
    margin: 0,
    matchingProp: "id",
    childProp: "children",
    nameProp: "name",
    editable: false,
    hasChildren: true
}

decorate(TreeNode, {
    expanded: observable,
})