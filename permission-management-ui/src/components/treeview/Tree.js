import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import TreeNode from "./TreeNode";

class Tree extends Component {


    constructor(props) {
        super(props);
    }


    render() {
        const {title, nodes, selected, options} = this.props;
        return (
                <div className="tree">
                    <div className="tree-title">
                        {title}
                    </div>
                    <div>
                    {nodes.map(node => <TreeNode {...options} key={node.id} node={node} selected={selected}/>)}
                    </div>
                </div>
        )
    }
}

export default observer(Tree)

Tree.propTypes = {
    nodes: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    title: PropTypes.string,
    selected: PropTypes.object,

    options: PropTypes.shape({
        hasChildren: PropTypes.bool,
        matchingProp: PropTypes.string,
        childProp: PropTypes.string,
        nameProp: PropTypes.string,
        childElements: PropTypes.func,
        onSelect: PropTypes.func.isRequired,
        editable: PropTypes.bool,
        onEdit: PropTypes.func
    })
}

Tree.defaultProps = {
    title: "Tree"
}
