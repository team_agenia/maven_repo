import red from "material-ui/colors/red";
import green from "material-ui/colors/green";

export const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

export const styleConst = {
    switchBase: {
        color: red[500],
        '& + $bar': {
            backgroundColor: red[500]
        },
        '&$checked': {
            color: green[500],
            '& + $bar': {
                backgroundColor: green[500],
            },
        },
    },
    bar: {},
    checked: {},
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
export const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};