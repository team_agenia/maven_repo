import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import {decorate, extendObservable, observable} from 'mobx';
import TextField from "material-ui/TextField/TextField";
import {
    Button,
    Chip,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    Input,
    InputLabel, MenuItem, Select, withStyles
} from "material-ui";
import {MenuProps, styles} from "./Theme";

class CreateUser extends Component {

    name = "";
    roles = [];

    constructor(props) {
        super(props);
    }

    handleChange = (selected) => {
        this.roles = selected.target.value.map(v => this.props.roles.find(r => r.id === v));
    };

    onCreate = () => {
        this.props.onCreate({
            name: this.name,
            roles: this.roles.map(r => r.json)
        })
    }

    render() {
        const {onClose, classes, theme, roles} = this.props;
        const {name} = this;
        const selectedRoles = this.roles;
        return (
            <Dialog open={true}>
                <DialogTitle>Create user</DialogTitle>
                <DialogContent>
                    <FormControl fullWidth className={classes.formControl}>
                    <TextField
                        label={"Name"}
                        value={name}
                        onChange={(evt) => this.name = evt.target.value}
                    />
                    </FormControl>
                    <FormControl fullWidth className={classes.formControl}>
                        <InputLabel htmlFor="select-multiple-chip">Roles</InputLabel>
                        <Select
                            multiple
                            value={selectedRoles.map(s => s.id)}
                            onChange={this.handleChange}
                            input={<Input id="select-multiple-chip"/>}
                            renderValue={selected => {
                                let list = selected.map(s => roles.find(r => r.id === s));
                                return (
                                    <div className={classes.chips}>
                                        {list.map(role => <Chip key={role.id} label={role.name}
                                                                    className={classes.chip}/>)}
                                    </div>
                                )
                            }}
                            MenuProps={MenuProps}
                        >
                            {roles.map(role => (
                                <MenuItem
                                    key={role.id}
                                    value={role.id}
                                    style={{
                                        fontWeight:
                                            selectedRoles.indexOf(role) === -1
                                                ? theme.typography.fontWeightRegular
                                                : theme.typography.fontWeightMedium,
                                    }}
                                >
                                    {role.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.onCreate}>Create</Button>,
                    <Button onClick={onClose}>Close</Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default withStyles(styles, {withTheme: true})(observer(CreateUser));

CreateUser.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    roles: PropTypes.object
}

CreateUser.defaultProps = {}

decorate(CreateUser, {
    name: observable,
    roles: observable
})