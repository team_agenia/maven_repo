import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import ResourceNode from "./ResourceNode";
import ResourceStore from "../stores/ResourceStore";

class ResourceOverview extends Component {


    constructor(props) {
        super(props);
    }


    render() {
        const {store} = this.props;
        const {resources} = this.props.store;
        return (
                <div className="resource-overview">
                    <div className="resource-overview-title">
                        Resources
                    </div>
                    <div>
                    {resources.map(resource => <ResourceNode key={resource.name} resource={resource} store={store}/>)}
                    </div>
                </div>
        )
    }
}

export default observer(ResourceOverview)

ResourceOverview.propTypes = {
    store: PropTypes.instanceOf(ResourceStore)
}
