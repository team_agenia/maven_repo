import PropTypes from "prop-types";
import React, {Component} from 'react';
import {decorate, observable} from "mobx";
import {observer} from "mobx-react";
import {Icon, IconButton, Switch, Tooltip} from "material-ui";

const iconSize = 24;

class TableRow extends Component {

    expanded = false;

    constructor(props) {
        super(props);
    }

    toggleExpand = () => {
        this.expanded = !this.expanded;
    }

    render() {

        const {
            element,
            selected,
            permissions,
            classes,
            onEdit,
            onUpdatePermission,
            hasPermission,
            isPermissionAvailable,
            childProp,
            elementSelectable,
        } = this.props;
        return (
            <li className="role-row" key={element.id}>
                <div className="role-row-content">
                    <IconButton variant="fab"
                                size="small"
                                aria-label="add"
                                style={{display: "inline-block", width: `${iconSize}px`, height: `${iconSize}px`}}
                                onClick={() => {
                                    this.toggleExpand()
                                }}>
                        <Icon>{this.expanded ? "arrow_drop_down" : "arrow_right"}</Icon>
                    </IconButton>
                    <div className={`text ${elementSelectable ? "selectable" : ""}`}
                         onClick={() => elementSelectable && onEdit(element)}>{element.name}</div>
                    <div className="permissions">
                        {permissions.map(permission => {
                            const granted = hasPermission(element, selected, permission);
                            const checked = !!granted;
                            const inherits = granted !== true && granted !== false && granted !== null;
                            //!!(permissionValues[role.id][permission.id]);
                            const sw = <Switch
                                key={`${element.id}-${permission.id}-switch`}
                                checked={checked}
                                disabled={!isPermissionAvailable(element, selected, permission)}
                                value={"" + permission.id}
                                onChange={(evt, checked) => {
                                    onUpdatePermission(element, selected, permission, checked);
                                }}
                                classes={classes}
                            />;
                            /*

                             */
                            return <div key={`${element.id}-${permission.id}`}>
                                {inherits ?
                                    <Tooltip key={`${element.id}-${permission.id}-tooltip`}
                                             title={`Access is inherited from '${granted}'`}
                                             placement="top"
                                             id={`${element.id}-${permission.id}-tooltip`}>
                                        {sw}
                                    </Tooltip> :
                                    sw
                                }
                            </div>
                        })}
                    </div>
                    {
                        this.expanded &&
                        <ul>
                            {element[childProp].map(el => <TableRow {...this.props} key={el.id}
                                                                    element={el}/>)}
                        </ul>
                    }
                </div>
            </li>)
    }

}

export default observer(TableRow);

TableRow.propTypes = {
    classes: PropTypes.object.isRequired,
    element: PropTypes.object,
    elementSelectable: PropTypes.bool,
    selected: PropTypes.object,
    permissions: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

    onEdit: PropTypes.func.isRequired,
    onUpdatePermission: PropTypes.func,
    hasPermission: PropTypes.func,
    isPermissionAvailable: PropTypes.func,

    childProp: PropTypes.string,
};

TableRow.defaultProps = {
    childProp: "children",
    elementSelectable: false,
};

decorate(TableRow, {
    expanded: observable
})