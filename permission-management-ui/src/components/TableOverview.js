import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FormControlLabel} from 'material-ui/Form';
import {Button, Tooltip} from 'material-ui';
import Switch from 'material-ui/Switch';
import green from 'material-ui/colors/green';
import red from 'material-ui/colors/red';
import {withStyles} from 'material-ui/styles';
import {observer} from "mobx-react";
import {decorate, observable} from "mobx";
import EditRole from "./EditRole";
import Resource from "../models/Resource";
import {styleConst} from "./Theme";
import TableRow from "./tableview/TableRow";


class TableOverview extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {title,
            permissions,
            list,
            selected,
            classes,
            rowOptions} = this.props;

        return (
            <div className="permission-overview" style={{width: "100%"}}>

                <div className="header">
                    <div style={{width: `calc(100% - ${permissions.length * 75}px)`}}>{title}</div>
                    {permissions.map(permission => <div key={permission.id}>{permission.name}</div>)}
                </div>
                <div className="body">
                    <ul>
                        {
                            list.map(element => {
                                return <TableRow {...rowOptions}
                                                 key={element.id}
                                                 selected={selected}
                                                 classes={classes}
                                                 element={element}
                                                 permissions={permissions}
                                                 />
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default (withStyles(styleConst)(observer(TableOverview)));






TableOverview.propTypes = {

    classes: PropTypes.object.isRequired,

    title: PropTypes.string,
    onSelectElement: PropTypes.func,
    onUpdateElement: PropTypes.func,
    onDeleteElement: PropTypes.func,


    permissions: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

    list: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    selected: PropTypes.object,

    rowOptions: PropTypes.shape({
        onEdit: PropTypes.func.isRequired,
        elementSelectable: PropTypes.bool,
        childProp: PropTypes.string,
        hasPermission: PropTypes.func,
        isPermissionAvailable: PropTypes.func,
        onUpdatePermission: PropTypes.func,
    })
}

TableOverview.defaultProps = {
    title: "list"
}

decorate(TableOverview, {
    selectedRole: observable
})
