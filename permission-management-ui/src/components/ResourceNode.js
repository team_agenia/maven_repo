import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import ResourceOverview from "./ResourceOverview";
import ResourceStore from "../stores/ResourceStore";
import {decorate, observable, reaction} from "mobx";
import {Icon, IconButton} from "material-ui";

const iconSize = 24;
class ResourceNode extends Component {

    expanded = false;
    hasChildren = true;

    constructor(props) {
        super(props);
        this.expanded = (props.resource.subResources || []).length > 0;
        reaction(() => this.expanded, (expanded) => {
            /*this.props.store.getElements(this.props.)*/
            if (this.props.resource.subResources.length === 0) this.hasChildren = false;
        })
    }

    toggleExpand = () => {
        this.expanded = !this.expanded;
    }

    render() {
        const {currentResource} = this.props.store;
        const active = currentResource && (currentResource.name === this.props.resource.name);
        const margin = this.props.margin + (this.hasChildren ? 0 : iconSize);
        return (
            <div className={"resource-node"}>
                <div className={"content" + (active ? " active" : "")}>
                    <div style={{marginLeft: `${margin}px`}}>
                        {this.hasChildren &&
                        <IconButton variant="fab"
                                    size="small"
                                    aria-label="add"
                                    style={{display: "inline-block", width: `${iconSize}px`, height: `${iconSize}px`}}
                                    onClick={() => {
                                        this.toggleExpand()
                                    }}>
                            <Icon>{this.expanded ? "remove" : "add"}</Icon>
                        </IconButton>
                        }
                        <div className={"text"} style={{width: `calc(100% - ${iconSize}px)` }}
                             onClick={() => this.props.store.currentResource = this.props.resource}>
                            {this.props.resource.name}
                        </div>
                    </div>
                </div>
                {this.expanded &&
                <div className="sub-resources">
                    {this.props.resource.subResources.map(resource => <ResourceNode key={resource.name}
                                                                                    margin={this.props.margin + 15}
                                                                                    resource={resource}
                                                                                    store={this.props.store}/>)}
                </div>
                }
            </div>
        )
    }
}

export default observer(ResourceNode)

ResourceNode.propTypes = {
    resource: PropTypes.object,
    margin: PropTypes.number,
    store: PropTypes.instanceOf(ResourceStore)
}

ResourceNode.defaultProps = {
    margin: 0
}

decorate(ResourceNode, {
    expanded: observable,
})